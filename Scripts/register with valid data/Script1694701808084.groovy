import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('Application/secondhand-24082023.apk', true)
//false untuk nanti appnya ga ke uninstall kalo true nanti ke uninstall
Mobile.waitForElementPresent(findTestObject('Object Repository/Page_secondhand/btn_Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Page_secondhand/btn_Akun'), 0)
//Mobile.tap(akun di home page, 0)
Mobile.tap(findTestObject('Object Repository/Page_akun/btn_masuk_akun'), 0)
//Mobile.tap(masuk akun page, 0)
Mobile.tap(findTestObject('Object Repository/Page_login/btn_daftar_login'), 0)
//Mobile.tap(daftar di login page, 0)

//isi data
Mobile.verifyElementVisible(findTestObject('Object Repository/Page_register/input_alamat'), 0)
Mobile.setText(findTestObject('Object Repository/Page_register/input_nama'), 'doni', 0)
Mobile.setText(findTestObject('Object Repository/Page_register/input_email'), 'ggg@test.com', 0)
Mobile.setText(findTestObject('Object Repository/Page_register/input_password'), '123456789', 0)
Mobile.setText(findTestObject('Object Repository/Page_register/input_noHP'), '08124563578', 0)
Mobile.setText(findTestObject('Object Repository/Page_register/input_kota'), 'bali', 0)
Mobile.setText(findTestObject('Object Repository/Page_register/input_alamat'), 'jalan sehat no 21', 0)
Mobile.tap(findTestObject('Object Repository/Page_register/btn_daftar'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/page_my_account/btn_logout'), 0)
//verify

