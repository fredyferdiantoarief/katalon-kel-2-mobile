import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

		Mobile.startApplication('Application/secondhand-24082023.apk', true)

		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_product/button_akun'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_akun'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/B_masuk'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/input_email'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/input_email'), 'i2fj02hdwj_ifewhj@gmail.com', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/input_password'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/input_password'), '123456', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/button_masuk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_masuk'), 0)	
		
//		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
//		
//				Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
		
//				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Product_1'), 0)
//		
//				Mobile.tap(findTestObject('Object Repository/Page_product/Product_1'), 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/jual'), 0)
				
						Mobile.tap(findTestObject('Object Repository/Page_product/jual'), 0)
				
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Nama_Produk'), 0)
		
				Mobile.setText(findTestObject('Object Repository/Page_product/Nama_Produk'), 'SmartTV Android bisa netplik', 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/harga'), 0)
		
				Mobile.setText(findTestObject('Object Repository/Page_product/harga'), '-1', 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Lokasi Produk'), 0)
		
				Mobile.setText(findTestObject('Object Repository/Page_product/Lokasi Produk'), 'Jakarta Pusat', 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)
		
				Mobile.tap(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/galeri'), 0)
		
				Mobile.tap(findTestObject('Object Repository/Page_product/galeri'), 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/gambar1'), 0)
		
				Mobile.tap(findTestObject('Object Repository/Page_product/gambar1'), 0)
				
						Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)
				
						Mobile.tap(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)
				
						Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/elektronik'), 0)
				
						Mobile.tap(findTestObject('Object Repository/Page_product/elektronik'), 0)
						
						Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Deskripsi'), 0)
						
								Mobile.setText(findTestObject('Object Repository/Page_product/Deskripsi'), '1kg detergen', 0)
		
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Terbitkan'), 0)
		
				Mobile.tap(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

						Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)
				
						Mobile.tap(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)
						
								Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
						
										Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)