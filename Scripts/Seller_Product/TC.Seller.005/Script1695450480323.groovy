import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


//given

Mobile.startApplication('Application/secondhand-24082023.apk', true)

Mobile.waitForElementPresent(findTestObject('Object Repository/SellerProduct/button_akun'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/button_akun'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/B_masuk'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/input_email'), 0)

Mobile.setText(findTestObject('Object Repository/SellerProduct/input_email'), 'i2fj02hdwj_ifewhj@gmail.com', 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/input_password'), 0)

Mobile.setText(findTestObject('Object Repository/SellerProduct/input_password'), '123456', 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/button_masuk'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/button_masuk'), 0)

//edit_product

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/Daftar_Jual_Saya'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/Daftar_Jual_Saya'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/button_produk1'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/button_produk1'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/edit_nama_produk'), 0)

Mobile.setText(findTestObject('Object Repository/SellerProduct/edit_nama_produk'), 'rinso', 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/edit_harga_produk'), 0)

Mobile.setText(findTestObject('Object Repository/SellerProduct/edit_harga_produk'), '100000', 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/edit_lokasi_produk'), 0)

Mobile.setText(findTestObject('Object Repository/SellerProduct/edit_lokasi_produk'), 'Jakarta', 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/input_FotoProduk'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/input_FotoProduk'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/tapfoto2'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/tapfoto2'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/img'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/img'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/button_Perbarui_Produk'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/button_Perbarui_Produk'), 0)

//then

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/button_produk1'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/button_produk1'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/Button_Back'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/Button_Back'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/SellerProduct/Daftar_Jual_Saya'), 0)

Mobile.tap(findTestObject('Object Repository/SellerProduct/Daftar_Jual_Saya'), 0)