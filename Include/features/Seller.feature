Feature: Seller

  Background: user in homepage
  Given user in homepage

  Scenario: user wants to add product with credentials
    When user click jual
    Then user successfully add product correctly

  Scenario: user wants to add product with minus price
    When user fill with minus price
    Then user successfully add product with minus price

  Scenario: user want to delete a product
    When user click product
    Then user successfully delete product

  Scenario: user want to see own product
    When user click daftar jual saya
    Then user successfully see own product

  Scenario: user wants to edit product
    When user click product
    Then user successfully change product