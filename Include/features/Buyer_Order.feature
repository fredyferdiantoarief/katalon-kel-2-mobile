@Buyer_Order

Feature: Buyer_Order
  
  Background: 
    Given user is in Beranda page
    
  Scenario: TC.Order.001 - User clicks on a product list
  	When user clicks on one product list
  	Then user is redirected to the Detail Product page
  	
  Scenario: TC.Order.002 - User can view details of Notifikasi page for a buyer account
  	When user clicks on the Notifikasi menu
  	Then user redirect to detail Notifikasi
  	
	Scenario: TC.Order.003 - User can Logout from the account and cannot offer a price for a product without Login
  	When user logout from the account
  	And user clicks on one product list
  	And user offers a price for a product
  	Then user redirect to Login page
  	
	Scenario: TC.Order.004 - User can view details of Pesanan Saya page for a buyer account
  	When user already Login
  	When user clicks on Pesanan Saya
  	Then user redirect to the Pesanan Saya page
  	
	Scenario: TC.Order.005 - User can search for a category by clicking on one of the category lists
  	When user clicks on one category
  	Then user can view the product list based on the chosen category