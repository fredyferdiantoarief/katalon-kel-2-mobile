package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver

import com.kms.katalon.core.util.KeywordUtil

public class Buyer_Order {

	@Given("user is in Beranda page")
	public void user_is_in_Beranda_page() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.delay(2)
	}

	@When("user clicks on one product list")
	public void user_clicks_on_one_product_list() {
		Mobile.delay(1)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/select_product'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/select_product'), 0)
	}

	@Then("user is redirected to the Detail Product page")
	public void user_is_redirected_to_the_Detail_Product_page() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Detail_Product/button_tertarik_ingin_nego'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Detail_Product/button_tertarik_ingin_nego'), 0)
	}

	@When("user clicks on the Notifikasi menu")
	public void user_clicks_on_the_Notifikasi_menu() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_notifikasi'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_notifikasi'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_notifikasi'), 0)
	}

	@Then("user redirect to detail Notifikasi")
	public void user_redirect_to_detail_Notifikasi() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/detail_notifikasi'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/detail_notifikasi'), 0)
	}

	@When("user logout from the account")
	public void user_logout_from_the_account() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_akun'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_akun'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_keluar'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_keluar'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_keluar'), 0)
		Mobile.delay(2)
	}

	@When("user offers a price for a product")
	public void user_offers_a_price_for_a_product() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Detail_Product/button_tertarik_ingin_nego'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Detail_Product/button_tertarik_ingin_nego'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Detail_Product/button_tertarik_ingin_nego'), 0)
		Mobile.delay(2)
	}

	@Then("user redirect to Login page")
	public void user_redirect_to_Login_page() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_masuk_belum_login'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_masuk_belum_login'), 0)
	}

	@When("user already Login")
	public void user_already_Login() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_akun'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_akun'), 0)
		Mobile.delay(2)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_pesanan_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_pesanan_saya'), 0)
	}

	@When("user clicks on Pesanan Saya")
	public void user_clicks_on_Pesanan_Saya() {
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/button_pesanan_saya'), 0)
		Mobile.delay(2)
	}

	@Then("user redirect to the Pesanan Saya page")
	public void user_redirect_to_the_Pesanan_Saya_page() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/detail_pesanan_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/detail_pesanan_saya'), 0)
	}

	@When("user clicks on one category")
	public void user_clicks_on_one_category() {
		Mobile.delay(1)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/carousel_banner'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/carousel_banner'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Buyer_Order/Beranda/select_category_elektronik'), 0)
	}

	@Then("user can view the product list based on the chosen category")
	public void user_can_view_the_product_list_based_on_the_chosen_category() {
		Mobile.delay(1)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_Buyer_Order/Beranda/select_product'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Buyer_Order/Beranda/select_product'), 0)
	}
}
