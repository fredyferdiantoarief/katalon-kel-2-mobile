package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.concurrent.Delayed

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile_User {

	@Given("user in home page")
	public void user_to_home_page() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_login'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_profile/Btn_Masuk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/tiitle_Masuk'), 0)
		Mobile.setText(findTestObject('Object Repository/Page_profile/android.widget.EditText - email'), 'e2@mail.com', 0)
		Mobile.setText(findTestObject('Object Repository/Page_profile/android.widget.EditText - password'), '12341234', 0)
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_Masuk_2'), 0)
		Mobile.delay(2)
	}

	@When("User click profile")
	public void user_profile() {
		Mobile.tap(findTestObject('Object Repository/Page_profile/icon-user-profile'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_profile/icon_pencil'),0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/title_Lengkapi-Info-Akun'), 0)
	}

<<<<<<< HEAD
//	@And("User input Name (.*)")
//	public void user_input_name(String name) {
//		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_set_value'), 0)
//		if(name.isEmpty()) {
//			Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
//			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/android.widget.TextView-Wajib -diisi'), 0)
//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
//			Mobile.delay(2)
//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
//			Mobile.delay(2)
//			Mobile.pressBack()
//		}else {
//			Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
//			Mobile.setText(findTestObject('Object Repository/Page_profile/textField-name'),name, 0)
//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
//		}
//		
//		Mobile.delay(2)
//		
//	}
=======
	//	@And("User input Name (.*)")
	//	public void user_input_name(String name) {
	//		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_set_value'), 0)
	//		if(name.isEmpty()) {
	//			Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
	//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
	//			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/android.widget.TextView-Wajib -diisi'), 0)
	//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
	//			Mobile.delay(2)
	//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
	//			Mobile.delay(2)
	//			Mobile.pressBack()
	//		}else {
	//			Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
	//			Mobile.setText(findTestObject('Object Repository/Page_profile/textField-name'),name, 0)
	//			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_nama'), 0)
	//		}
	//
	//		Mobile.delay(2)
	//
	//	}
>>>>>>> d86b98ac1f1750100ac7f0036308eec10665e54c

	@And("User input Kota (.*)")
	public void user_input_kota(String kota) {
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_set_kota'), 0)
		if(kota.isEmpty()) {
			Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/android.widget.TextView-Wajib -diisi'), 0)
			Mobile.delay(2)
			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_'), 0)
			Mobile.delay(2)
			Mobile.pressBack()
		}else {
			Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
			Mobile.setText(findTestObject('Object Repository/Page_profile/textField-name'),kota, 0)
			Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_'), 0)
		}
	}

	@And("User input Alamat (.*)")
	public void user_input_alamat(String alamat) {
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_set_alamat'), 0)
		Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
		Mobile.setText(findTestObject('Object Repository/Page_profile/textField-name'),alamat, 0)
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_'), 0)
	}

	@And("User input Hanphone (.*)")
	public void user_redirect_profile_page(String no_hp) {
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_set_number_phone'), 0)
		Mobile.clearText(findTestObject('Object Repository/Page_profile/textField-name'), 0)
		Mobile.setText(findTestObject('Object Repository/Page_profile/textField-name'),no_hp, 0)
		Mobile.tap(findTestObject('Object Repository/Page_profile/btn_simpan_'), 0)
	}

	@Then("User (.*) update profile (.*)")
	public void user_status_update_profile(String expect,String test_case_id) {
		if(expect == "success") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/title_Lengkapi-Info-Akun'), 0)
		}
		if(expect == "fail") {
			if(test_case_id == "TC.Profile.003" ) {
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/title_Lengkapi-Info-Akun'), 0)
			}
			if(test_case_id == "TC.Profile.005") {
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/title_Lengkapi-Info-Akun'), 0)
			}
			if(test_case_id != "TC.Profile.005" &&  test_case_id != "TC.Profile.003") {
				Mobile.verifyElementVisible(findTestObject('Object Repository/Page_profile/title_Lengkapi-Info-Akun'), 0)
			}
		}
	}
}